const ACTIVATION_FUNCTIONS = {
    hs: function heavySide(n, th) {
        th = th || 0;
        if (n >= th) {
            return 1;
        }
        else {
            return 0;
        }
    },
    sig: function sigmoid(n) {
        return 1 / (1 + Math.pow(Math.E, -n));
    },
    reLu: function reLu(n) {
        return Math.max(0, n);
    },
    tanH: function tanH(n) {
        return Math.tanh(n);
    },
    sgn: function sign(n) {
        return Math.sign(n);
    }
};

class SimplePerceptron {
    /**
     * Initializes a SimplePerceptron Instance
     * @param {number} inputLength - The length of input values. Default 1
     * @returns {SimplePerceptron} - Simple Perceptron Instance who can solve all types of linear problems
     */
    constructor(inputLength) {
        const self = this;
        self.inputLayers = inputLength || 1;
        self.weights = [];
        self.input = Array(self.inputLayers);
        self.output = 1;
        self.bias = 1;
        self.learningRate = 0.01;
        self.activation = 'hs';

        for (let i = 0; i < self.inputLayers + 1; i++) {
            self.weights.push(random(-1, 1)); //Initializing random weights between -1 and 1
        }
        return self;
    }

    /**
     * Guesses a value for a given input
     * @param {Array<number>} inputsArray
     * @returns {number}
     */
    guess(inputsArray) {
        const self = this;
        let sum = 0;
        let inputLength = inputsArray.length;
        for (let i = 0; i < inputLength + 1; i++) { // calculating scalar product of weights and inputs
            if (i < inputLength) {
                sum += (self.weights[i] * inputsArray[i]);
            }
            else {
                sum += self.weights[i] * self.bias;
            }
        }
        // console.log('Sigma:', sum);
        return self.activationFunction(sum);
    }

    /**
     * Call the activation function and returns the output of this. As default `heavySide`-Function
     * @param x
     * @returns {number}
     */
    activationFunction(x) {
        if (!this.activation) {
            return x;
        }
        else {
            return ACTIVATION_FUNCTIONS[this.activation](x);
        }
    }

    /**
     * Fits a single input in depends of his real output
     * @param {Array<number>} inputToGuess
     * @param {number} realOutput
     * @returns {void}
     */
    train(inputToGuess, realOutput) {
        const self = this;
        let guessed = self.guess(inputToGuess);
        let error = realOutput - guessed;
        for (let i = 0; i < self.weights.length; i++) {
            if (i < self.inputLayers) {
                self.weights[i] += (error * inputToGuess[i] * self.learningRate); // fitting weight for input values
            }
            else {
                self.weights[i] += (error * self.bias * self.learningRate); // fitting weight for bias
            }
        }
    }

    /**
     * Form in this case will be:
     * Perceptron = ∑(W,X) + B*wB > 0
     = x1*w1 + x2*w2 + B*Wb > 0
     = x1*w1 + x2*w2 > - B*Wb
     =  x2 > (-B*Wb - x1*w1)/ w2
     =  x2 >  -x1*w1/w2 - B*Wb/ w2
     = x2=Y >  -(w1/w2)*X - B*Wb/ w2
     */

    /**
     * Gets the Y value of a X number
     * @param {number} x
     * @returns {number} - `slope * X + intercept`
     */
    getLine(x) {
        const self = this;
        let m = -(self.weights[0] / self.weights[1]); // calculating real slope
        let q = -(self.bias * self.weights[2]) / self.weights[1]; //calculating intercept

        return m * x + q;
    }

    /**
     * Returns the form of the perceptron's line
     * @returns {string} - `m{number}* x{string} + q{number}`
     */
    getLineString() {
        const self = this;
        let m = -(self.weights[0] / self.weights[1]); // calculating real slope
        let q = -(self.bias * self.weights[2]) / self.weights[1]; //calculating intercept

        return m + '*x + ' + q;
    }

    /**
     * Draws Line of perceptron on Canvas-Context
     * @returns {boolean}
     */
    drawLine() {
        const self = this;
        console.log('Perceptron Line:', self.getLineString());
        drawLine([0, self.getLine(0)], [canvas.width, self.getLine(canvas.width)], 2, 'purple');
        return true;
    }
}