class Point {
    /**
     * Initializes a point on Canvas-Context
     * @param {number} _x - The X-Coordinate on Canvas-Context. Default random number between the begin and end of canvas width
     * @param {number} _y - The Y-Coordinate on Canvas-Context. Default random number between the begin and end of canvas height
     * @param {number} width
     * @param {number} height
     * @namespace
     * @prop {number} Point.x
     * @prop {number} Point.y
     * @prop {number} Point.width
     * @prop {number} Point.height
     * @prop {number} Point.label
     * @prop {string} Point.color
     * @prop Point.draw()
     * @returns {Point} - Point Instance
     */
    constructor(_x, _y, width, height) {
        let self = this;
        self.x = (parseInt(_x + '').toString() !== 'NaN') ? _x : Math.floor(random(1, canvas.width - 1));
        self.y = (parseInt(_y + '').toString() !== 'NaN') ? _y : Math.floor(random(1, canvas.height - 1));
        self.width = width || 2;
        self.height = height || 2;
        let tmpY = getPointCoordinate(lineOrigin, lineEnd, self.x).y;

        if (Math.floor(tmpY) === self.y) return new Point(null, null, self.width, self.height); //Avoid points on the line
        self.label = ACTIVATION_FUNCTIONS.hs(self.y, tmpY);

        return self;
    }

    /**
     * Draws point instance on Canvas-Context
     * @param {string||null} color - The color of the point. As default will be blue if `point.label=1` else green
     * @returns {Point}
     */
    draw(color) {
        const self = this;
        if (self.label) {
            self.color = 'blue';
        }
        else {
            self.color = 'green'
        }
        ctx.save();
        ctx.fillStyle = color || self.color;
        ctx.fillRect(self.x, self.y, self.width, self.height);
        ctx.restore();
        ctx.stroke();

        return self;
    }
}