This is a demo I made for a presentation to explain the basic concept of a Perceptron.

If you want to try the demo, simply download the project and open the file "index.html" with your favorite browser.

It will show a line running through a canvas.
The line is generated randomly each time the page is refreshed.

Then 1000 points will be created in the canvas, which will be colored green or blue depending on their position referring to the line.

If you want to use more points for the demo, then add "#NrOfPoints" to the URL.

For example: index.html#3500 to have 3500 points on the canvas.

N.B.: If you want more info, then open the console. 


@Tested with Chrome