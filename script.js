function random(min, max) {
    return Math.random() * (max - min) + min;
}

function getPointCoordinate(X, Y, reference) {
    let [x1, y1] = X;
    let [x2, y2] = Y;
    let x, y;
    if (x1 !== x2 && y1 !== y2) {
        let m = (y1 - y2) / (x1 - x2);
        let q = ((x1 * y2) - (x2 * y1)) / (x1 - x2);

        x = x1;
        y = m * reference + q;
    }
    else {
        x = x1;
        y = y1;
    }
    return {x: x, y: y}
}

function getLineForm(X, Y) {
    let [x1, y1] = X;
    let [x2, y2] = Y;

    let m = (y1 - y2) / (x1 - x2);
    let q = ((x1 * y2) - (x2 * y1)) / (x1 - x2);

    return m + '*x + ' + q;
}

function drawLine(Origin, End, size, color) {
    let x1 = Origin[0];
    let y1 = Origin[1];
    let x2 = End[0];
    let y2 = End[1];

    //ctx.save();
    ctx.strokeStyle = color || 'black';
    ctx.lineWidth = size || 2;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.restore();
}

function createContext(pointsLength) {
    drawLine(lineOrigin, lineEnd, 2);

    pointsLength = pointsLength || 100;
    points = Array(pointsLength);
    let labeled = 0;
    for (let i = 0; i < points.length; i++) {
        points[i] = new Point();
        points[i].draw();
        if (points[i].label) labeled++;
    }
    console.log('Created', pointsLength, 'points on canvas!');
    console.log('Blue points are:', labeled);
}

function initializePerceptron() {
    p = new SimplePerceptron(2);
    guessAllPoints();
}

function guessAllPoints() {
    for (let i = 0; i < points.length; i++) {
        points[i].draw('grey');
        let input = [points[i].x, points[i].y];
        let output = points[i].label;
        setTimeout(function () {
            let guessed = p.guess(input);
            if (guessed !== output) {
                points[i].draw('red');
            }
            else {
                points[i].draw();
            }
        }, 1000);
    }
}

function singleTrain() {
    for (let j = 0; j < points.length; j++) {
        let input = [points[j].x, points[j].y];
        let output = points[j].label;
        p.train(input, output);
    }
    return true;
}

async function trainPerceptron(times) {
    times = times || 1;

    for (let i = 0; i < times; i++) {
        await singleTrain();
    }
}

function animateAfterTrain() {
    console.log('Trained!');
    for (let i = 0; i < points.length; i++) {
        let input = [points[i].x, points[i].y];
        let output = points[i].label;
        setTimeout(function () {
            let guessed = p.guess(input);
            if (guessed !== output) {
                points[i].draw('red');
            }
            else {
                points[i].draw();
            }
        }, 1000);
    }

    return true;
}

const btnInitialize = document.getElementById('initializerJs');
const btnPerceptron = document.getElementById('trainPerceptron');
const btnDrawPerceptron = document.getElementById('drawLine');
const btnGuessAll = document.getElementById('guessAll');
// const consDiv = document.getElementById('console');
const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

//Setting random line
const lineOrigin = [0, parseFloat(random(0, canvas.height).toFixed(0))];
const lineEnd = [canvas.width, parseFloat(random(0, canvas.height).toFixed(0))];

let points; // points on canvas
let p; // perceptron

console.log('Starting from:', lineOrigin);
console.log('to:', lineEnd);
console.log('Line:', getLineForm(lineOrigin, lineEnd));


window.addEventListener('DOMContentLoaded', function initAll() {
    const needToInitPerceptron = 'Need to initialize the Perceptron!';
    let pointsNr = parseInt((window.location.hash).replace('#', '')) || 1000; //getting points number from hash-value on link

    createContext(pointsNr);

    btnInitialize.addEventListener('click', function init() {
        console.log('Initializing Perceptron');
        initializePerceptron();
    });

    btnPerceptron.addEventListener('click', function startTrain() {
        if (p) {
            let times = document.getElementById('nrTrainJs').value;
            times = times || 1;
            console.log('Training', times, 'times');
            return trainPerceptron(times)
                .then(animateAfterTrain);
        }
        else {
            throw new Error(needToInitPerceptron);
        }
    });

    btnDrawPerceptron.addEventListener('click', function drawLineOfPerceptron() {
        if (p) {
            p.drawLine();
        }
        else {
            throw new Error(needToInitPerceptron);
        }
    });

    btnGuessAll.addEventListener('click', function guessAllPointsOnCanvas() {
        if (p) {
            guessAllPoints();
        }
        else {
            throw new Error(needToInitPerceptron);
        }
    });
});


